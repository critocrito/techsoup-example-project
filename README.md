# Example Littlefork Project

## Requirements

In order to run Littlefork, [NodeJS](https://nodejs.org/en/download/) needs to
be installed. If using Linux, there are
[instructions](https://nodejs.org/en/download/package-manager/) to install it
using a package manager. Most releases will work, but it has been tested with
NodeJS version higher than 7.5.

All the instructions assume you are working in a terminal. Those instructions
are tested on Linux and MacOS.

## Installation

Clone this example project. If you have `git` installed, simply clone the
repository:

    git clone https://gitlab.com/critocrito/techsoup-example-project.git

Install the dependencies using `npm`:

    cd techsoup-example-project
    npm install

## Fetching Instagram Feeds

To manually fetch a single Instagram feed, use the following command:

    node_modules/.bin/littlefork -Q instagram_user:poroshenkopetro \
                                 -p instagram_feed,tap_printf

This will fetch 100 posts from the Instagram feed of the user
`poroshenkopetro` and print it's result to the screen.

To control how many posts to fetch, use the `--instagram.post_count`
option, otherwise it downloads 100 posts as a default.

    node_modules/.bin/littlefork -Q instagram_user:poroshenkopetro \
                                 -p instagram_feed,tap_printf \
                                 --instagram.post_count 200

To download multiple feeds, download all images and export the data as a CSV,
use the following command:

    node_modules/.bin/littlefork -Q instagram_user:poroshenkopetro,instagram_user:andrzej.duda \
                                 -p instagram_feed,http_get,csv_export

Those data processes can be codified, by populating JSON text files with the
search queries, and the data pipeline configuration. There are two examples in
the same project.

    node_modules/.bin/littlefork -c instagram.json -q queries.json

## Fetching Facebook Pages

In order to use the Facebook plugins, one needs an `app_id` and an
`app_secret.` Those can be obtained from the [App
Dashboard](https://developers.facebook.com/apps).

To manually fetch a single Facebook page, run Littlefork as follows:

    node_modules/.bin/littlefork -Q facebook_page:SPD \
                                 -p facebook_api_pages,tap_printf \
                                 --facebook.app_id <your id> \
                                 --facebook.app_secret <your secret>

As with the Instagram plugins, one can write the configuration and queries
into a JSON file. Edit `facebook.json` and fill in your `app_id` and
`app_secret`.

    node_modules/.bin/littlefork -c facebook.json -q queries.json

## Fetching Facebook Users

It works the same as with Facebook pages, only the plugin is called
`facebook_api_users`.
